package model;

import java.util.Date;

public class Repairer extends User {
    private int expiriance;
    private Date admissionDate;
    
    public Repairer(String name, String surname, String phone, String email,
            String login, String password, int expiriance, Date admissionDate){
        super(name, surname, phone, email, login, password);
        this.expiriance = expiriance;
        this.admissionDate = admissionDate;
    }
    public Repairer(Long id, String name, String surname, String phone, String email,
            String login, String password, int expiriance, Date admissionDate){
        super(id, name, surname, phone, email, login, password);
        this.expiriance = expiriance;
        this.admissionDate = admissionDate;
    }
    
    public int getExpiriance(){
        return this.expiriance;
    }
    public Date getAdmissionDate(){
        return this.admissionDate;
    }
    
    public void setExpiriance(int expiriance){
        this.expiriance = expiriance;
    }
    public void setAdmissionDate(Date admissionDate){
        this.admissionDate = admissionDate;
    }
}
