package model;

public class Additional extends Entity {
    private String name;
    private double price;
    private String description;
    
    public Additional(String name, double price, String description){
        this.name = name;
        this.price = price;
        this.description = description;
    }
    public Additional(Long id, String name, double price, String description){
        super(id);
        this.name = name;
        this.price = price;
        this.description = description;
    }
    
    public String getName(){
        return this.name;
    }
    public double getPrice(){
        return this.price;
    }
    public String getDescription(){
        return this.description;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public void setPrice(double price){
        this.price = price;
    }
    public void setDescription(String description){
        this.description = description;
    }
    
    public String toString(){
        return this.name + " " + this.price;
    }
    
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(!(object instanceof Additional)){
            return false;
        }
        Additional additional = (Additional)object;
        return this.id == additional.getId()
                && this.name != null && this.name.equals(additional.getName())
                && this.price == additional.getPrice();
    }
    public int hashCode(){
        int hashCode = 67;
        hashCode = hashCode * 67 + this.id.hashCode();
        hashCode = (this.name != null) ? hashCode * 67 + this.name.hashCode() : hashCode;
        hashCode = hashCode * 67 + Double.valueOf(this.price).hashCode();
        return hashCode;
    }
}
