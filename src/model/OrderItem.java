package model;

import java.util.ArrayList;

public class OrderItem extends Entity {
    private String name;
    private String malefunction;
    private String description;
    private double price;
    private ArrayList<Service> services;
    private ArrayList<Additional> additionals;
    
    public OrderItem(String name, String malefunction, String description, double price){
        this.name = name;
        this.malefunction = malefunction;
        this.description = description;
        this.price = price;
        this.services = new ArrayList<>();
        this.additionals = new ArrayList<>();
    }
    public OrderItem(String name, String malefunction, String description, double price,
            ArrayList<Service> services, ArrayList<Additional> additionals){
        this.name = name;
        this.malefunction = malefunction;
        this.description = description;
        this.price = price;
        this.services = services;
        this.additionals = additionals;
    }
    public OrderItem(OrderItemId id, String name, String malefunction, String description, double price){
        super(id);
        this.name = name;
        this.malefunction = malefunction;
        this.description = description;
        this.price = price;
        this.services = new ArrayList<>();
        this.additionals = new ArrayList<>();
    }
    public OrderItem(OrderItemId id, String name, String malefunction, String description, double price,
            ArrayList<Service> services, ArrayList<Additional> additionals){
        super(id);
        this.name = name;
        this.malefunction = malefunction;
        this.description = description;
        this.price = price;
        this.services = services;
        this.additionals = additionals;
    }
    
    public String getName(){
        return this.name;
    }
    public String getMalefunction(){
        return this.malefunction;
    }
    public String getDescription(){
        return this.description;
    }
    public double getPrice(){
        return this.price;
    }
    public ArrayList<Service> getServices(){
        return this.services;
    }
    public ArrayList<Additional> getAdditionals(){
        return this.additionals;
    }
    
    public void setName(String name){
        this.name = name;
    }
    public void setMalefunction(String malefunction){
        this.malefunction = malefunction;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public void setPrice(double price){
        this.price = price;
    }
    public void setServices(ArrayList<Service> services){
        this.services = services;
    }
    public void setAdditionals(ArrayList<Additional> additioanls){
        this.additionals = additioanls;
    }
    
    public boolean addService(Service service){
        return this.services.add(service);
    }
    public boolean removeService(Service service){
        return this.services.remove(service);
    }
    
    public String toString(){
        return this.name + " " + this.malefunction + " " + this.price;
    }
    
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(!(object instanceof OrderItem)){
            return false;
        }
        OrderItem orderItem = (OrderItem)object;
        return this.id == orderItem.getId();
    }
    public int hashCode(){
        int hashCode = 67;
        hashCode = (this.id != null) ? hashCode * 67 + this.id.hashCode() : hashCode;
        return hashCode;
    }
}
