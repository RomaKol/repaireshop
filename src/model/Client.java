package model;

import java.util.Date;

public class Client extends User {
    private Date registrationDate;
    
    public Client(String name, String surname, String phone, String email,
            String login, String password, Date registrationDate){
        super(name, surname, phone, email, login, password);
        this.registrationDate = registrationDate;
    }
    public Client(Long id, String name, String surname, String phone, String email,
            String login, String password, Date registrationDate){
        super(id, name, surname, phone, email, login, password);
        this.registrationDate = registrationDate;
    }
    
    public Date getRegistrationDate(){
        return this.registrationDate;
    }
    
    public void setRegistrationDate(Date registrationDate){
        this.registrationDate = registrationDate;
    }
}
