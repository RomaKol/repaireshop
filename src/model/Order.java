package model;

import java.util.Date;
import java.util.List;

public class Order extends Entity {
    private Client client;
    private Repairer repairer;
    private Date date;
    private String repairerComment;
    private List<OrderItem> orderItems;
}
