package model;

public class User extends Entity {
    protected String name;
    protected String surname;
    protected String phone;
    protected String email;
    protected String login;
    protected String password;
    
    public User(String name, String surname, String phone, String email,
            String login, String password){
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.login = login;
        this.password = password;
    }
    public User(Long id, String name, String surname, String phone, String email,
            String login, String password){
        super(id);
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.login = login;
        this.password = password;
    }

    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getPhone() {
        return this.phone;
    }
    public String getEmail() {
        return this.email;
    }
    public String getLogin() {
        return this.login;
    }
    public String getPassword() {
        return this.password;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String toString(){
        return this.name + " " + this.surname;
    }
    
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(!(object instanceof User)){
            return false;
        }
        User user = (User)object;
        
        return this.id == user.getId()
                && this.name != null && this.name.equals(user.getName());
    }
    public int hashCode(){
        int hashCode = 31;
        hashCode = 31 * hashCode + this.id.hashCode();
        hashCode = (this.name != null) ? 31 * hashCode + this.name.hashCode(): hashCode;
        return hashCode;
    }
}
