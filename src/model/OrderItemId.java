package model;

public class OrderItemId {
    private Long orderId;
    private Long serviceId;
    
    public OrderItemId(Long orderId, Long serviceId){
        this.orderId = orderId;
        this.serviceId = serviceId;
    }
    
    public Long getOrderId(){
        return this.orderId;
    }
    public Long getServiceId(){
        return this.serviceId;
    }
    
    public void setOrderId(Long orderId){
        this.orderId = orderId;
    }
    public void setServiceId(Long serviceId){
        this.serviceId = serviceId;
    }
    
    public String toString(){
        return this.orderId + " " + this.serviceId;
    }
    
    public boolean equals(Object object){
        if(this == object){
            return true;
        }
        if(!(object instanceof OrderItemId)){
            return false;
        }
        OrderItemId orderItemId = (OrderItemId)object;
        return this.orderId == orderItemId.getOrderId()
                && this.serviceId == orderItemId.getServiceId();
    }
    public int hashcode(){
        int hashCode = 31;
        hashCode = hashCode * 31 + this.orderId.hashCode();
        hashCode = hashCode * 31 + this.serviceId.hashCode();
        return hashCode;
    }
}
